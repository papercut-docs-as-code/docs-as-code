.. index::
    single: Redoc

=========================
Using the Redoc generator
=========================

`Redocly <https://redoc.ly/>`_ provide various tools for OAS docs, including
the `redoc <https://github.com/Redocly/redoc>`_ tool.

Like most tools Redoc uses dynamic web page programming to render the OAS content and
there are a variety of ways to deploy it.
We will take the simple approach provided `here <https://github.com/Redocly/redoc#user-content-tldr>`_.

For this demonstration we will use an existing Hugo
`project <https://gitlab.com/papercut-docs-as-code/hugo-and-openapi>`_.

You can find the OpenAPI specification rendered separately on this `page <./_static/redoc.html>`_
